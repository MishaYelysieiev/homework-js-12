addBtn = document.getElementsByClassName('button')[0];
addBtn.id = "add";

let inputDiam = document.createElement('input');
let inputVal = inputDiam.value;
let createBtn = document.createElement('button');

document.addEventListener('click',(e)=>{
    inputDiam.placeholder = "enter the diameter";
    createBtn.innerText = "Draw circles"
    createBtn.classList.add('button');
    createBtn.id = "create";
    
    if(e.target.id === "add") {
        addBtn.style.display="none";
        document.body.appendChild(inputDiam);
        document.body.appendChild(createBtn);  
    }if(e.target.id === "create") {
        const simpleDiv = document.createElement('div');
        simpleDiv.style = "display:flex;flex-wrap:wrap;";
        document.body.appendChild(simpleDiv);
        
        for(let i=0;i<100;i++) {
            let circle = document.createElement('div');
            circle.class = "circle";
            let letters = '0123456789ABCDEF';
            let color = "#";
            for (let k = 0; k < 6; k++) {
                color +=letters[Math.floor(Math.random() * 16)];
            };
            circle.style.backgroundColor=color;
            circle.style.width = inputDiam.value + "px";
            circle.style.height = inputDiam.value + "px";
            circle.style.margin = "10px";
            circle.style.borderRadius = "50%";
            simpleDiv.appendChild(circle);
        };
    }if(e.target.class==="circle"){
        e.target.style.opacity="0";
    }
});



